=======================
Neutron FWaaS Dashboard
=======================

.. warning::
    Due to lack of maintainers this project is now deprecated in the Neutron
    stadium and will be removed from stadium in ``W`` cycle.
    If You want to step in and be maintainer of this project to keep it in the
    Neutron stadium, please contact the ``neutron team`` via
    openstack-discuss@lists.openstack.org or IRC channel #openstack-neutron
    @freenode.

OpenStack Dashboard panels for Neutron FWaaS

* Documentation: https://docs.openstack.org/neutron-fwaas-dashboard/latest/
* Source: https://opendev.org/openstack/neutron-fwaas-dashboard
* Bugs: https://bugs.launchpad.net/neutron-fwaas-dashboard
* Release Notes: https://docs.openstack.org/releasenotes/neutron-fwaas-dashboard/
